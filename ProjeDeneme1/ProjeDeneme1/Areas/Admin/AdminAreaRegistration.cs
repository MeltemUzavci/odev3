﻿using System.Web.Mvc;

namespace ProjeDeneme1.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{lang}/{controller}/{action}/{id}",
                new { lang="tr",action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
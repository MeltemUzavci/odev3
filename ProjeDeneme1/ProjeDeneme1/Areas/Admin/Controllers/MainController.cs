﻿using ProjeDeneme1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjeDeneme1.Areas.Admin.Controllers
{
   
    [Authorize(Roles ="Admin")]
    [Language]
    public class MainController : Controller
    {
        // GET: Admin/Main
        private performansEntities db = new performansEntities();


        public ActionResult Index()
        {
            var bilgi =
                  from cb in db.CalisanBilgileri
                  join pp in db.performansPuan
                  on cb.CalisanID equals pp.CalisanID
                  join cu in db.CalisanUnvan
                  on cb.CalisanUnvanID equals cu.CalisanUnvanID
                  join pa in db.PerformansAciklama
                  on pp.IsID equals pa.IsID
                  where cb.CalisanDurum == true
                  select new CalisanViewModel { CalisanBilgileri = cb, performansPuan = pp, CalisanUnvan = cu, PerformansAciklama = pa };


            return View(bilgi);

        }
        public ActionResult CalisanDuzenle()
        {
            var calisanBilgileri =
               from cb in db.CalisanBilgileri
               join cu in db.CalisanUnvan
               on cb.CalisanUnvanID equals cu.CalisanUnvanID
               where cb.CalisanDurum == true
               select new CalisanViewModel { CalisanBilgileri = cb, CalisanUnvan = cu };


            return View(calisanBilgileri);
        }

        public ActionResult Duzenle()
        {
            return View();
        }


        public ActionResult CalisanSil()
        {
            //var calisanBilgileri = db.CalisanBilgileri.Include(c => c.CalisanUnvan);
            var calisanBilgileri =
                from cb in db.CalisanBilgileri
                join cu in db.CalisanUnvan
                on cb.CalisanUnvanID equals cu.CalisanUnvanID
                where cb.CalisanDurum == true
                select new CalisanViewModel { CalisanBilgileri = cb, CalisanUnvan = cu };


            return View(calisanBilgileri);
        }
        public ActionResult Sil()
        {

            return View();
        }


        public ActionResult IsEkle()
        {
            var bilgi =
                from cb in db.CalisanBilgileri
                join pp in db.performansPuan
                on cb.CalisanID equals pp.CalisanID
                join cu in db.CalisanUnvan
                on cb.CalisanUnvanID equals cu.CalisanUnvanID
                where cb.CalisanDurum == true
                select new CalisanViewModel { CalisanBilgileri = cb, performansPuan = pp, CalisanUnvan = cu };

            return View(bilgi);

        }
        public ActionResult Iekle()
        {
            return View();
        }

        public ActionResult IsGuncelle()
        {
            var bilgi =
                 from cb in db.CalisanBilgileri
                 join pp in db.performansPuan
                 on cb.CalisanID equals pp.CalisanID
                 join cu in db.CalisanUnvan
                 on cb.CalisanUnvanID equals cu.CalisanUnvanID
                 where cb.CalisanDurum == true
                 select new CalisanViewModel { CalisanBilgileri = cb, performansPuan = pp, CalisanUnvan = cu };

            return View(bilgi);
        }
        public ActionResult Iguncelle()
        {
            return View();
        }
    }
}
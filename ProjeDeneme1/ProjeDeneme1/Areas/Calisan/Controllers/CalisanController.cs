﻿using ProjeDeneme1.Models;
using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjeDeneme1.Areas.Calisan.Controllers
{
    [Authorize(Roles ="Calisan")]
    public class CalisanController : Controller
    {
        private performansEntities db = new performansEntities();
        // GET: Calisan/Calisan
        public ActionResult Index()
        {
            var bilgi =
                from cb in db.CalisanBilgileri
                join pp in db.performansPuan
                on cb.CalisanID equals pp.CalisanID
                join cu in db.CalisanUnvan
                on cb.CalisanUnvanID equals cu.CalisanUnvanID
                join pa in db.PerformansAciklama
                on pp.IsID equals pa.IsID
                where cb.CalisanDurum == true && pp.CalisanID == 4 && pa.AciklamaID == 3
                select new CalisanViewModel { CalisanBilgileri = cb, performansPuan = pp, CalisanUnvan = cu, PerformansAciklama = pa };

            return View(bilgi);
        }
        public ActionResult Is()
        {
            var sorgu = from pp in db.performansPuan where pp.CalisanID == 4 select pp;
            return View(sorgu);
        }
        public ActionResult Puan()
        {
            var sorgu = from pp in db.performansPuan where pp.CalisanID == 4 select pp;
            return View(sorgu);
        }
        public ActionResult Aciklama()
        {
            var sorgu = from pa in db.PerformansAciklama where pa.AciklamaID == 3 select pa;
            return View(sorgu);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjeDeneme1.Models
{
    public class CalisanViewModel
    {
        public CalisanBilgileri CalisanBilgileri { get; set; }
        public performansPuan performansPuan { get; set; }
        public CalisanUnvan CalisanUnvan { get; set; }
        public PerformansAciklama PerformansAciklama { get; set; }

    }
}